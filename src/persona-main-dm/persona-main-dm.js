import { LitElement } from 'lit-element';

class PersonaMainDM extends LitElement {

    static get properties() {
        return {
            people: { type: Array }
        };
    }

    constructor() {
        super();

        this.people = [
            {
                name: 'Natos',
                yearsInCompany: 10,
                profile: "Lorem Ipsum is simply dummy text of the printing and typesetting",
                photo: {
                    src: './img/Natos.jpg',
                    alt: 'Natos'
                }
            }, {
                name: 'Waor',
                yearsInCompany: 7,
                profile: "industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley",
                photo: {
                    src: './img/Waor.jpg',
                    alt: 'Waor'
                }
            }, {
                name: 'Recycled J',
                yearsInCompany: 3,
                profile: "of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in",
                photo: {
                    src: './img/RecycledJ.jpg',
                    alt: 'Recycled J'
                }
            }, {
                name: 'Ayax',
                yearsInCompany: 6,
                profile: "the 1960s with the release of Letraset sheets containing Lorem",
                photo: {
                    src: './img/Ayax.jpg',
                    alt: 'Ayax'
                }
            }, {
                name: 'Prok',
                yearsInCompany: 2,
                profile: "Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
                photo: {
                    src: './img/Prok.jpg',
                    alt: 'Prok'
                }
            }
        ];

        this.showPersonForm = false;
    }

    updated(changedProperties) {
        console.log('persona-main-dm - updated');
        if(changedProperties.has('people')) {
            console.log('persona-main-dm - updated - Ha cambiado la propiedad people');

            this.dispatchEvent(new CustomEvent('updated-people', {
                detail: {
                    people: this.people
                }
            }));
        }
    }
}

customElements.define('persona-main-dm', PersonaMainDM);
