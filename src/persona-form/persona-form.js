import { LitElement, html } from 'lit-element';

class PersonaForm extends LitElement {

    static get properties() {
        return {
            person: { type: Object },
            editingPerson: { type: Boolean }
        };
    }

    constructor() {
        super();
        this.resetFormData();
    }

    render() {
        return html `
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
            <div>
                <form>
                    <div class="form-group">
                        <label>Nombre Completo</label>
                        <input type="text" class="form-control" placeholder="Nombre Completo"
                            @input="${this.updateName}"
                            .value="${this.person.name}"
                            ?disabled="${this.editingPerson}" />
                    </div>
                    <div class="form-group">
                        <label>Perfil</label>
                        <textarea class="form-control" placeholder="Perfil" rows="5"
                            @input="${this.updateProfile}"
                            .value="${this.person.profile}"></textarea>
                    </div>
                    <div class="form-group">
                        <label>Años en la empresa</label>
                        <input type="text" class="form-control" placeholder="Años en la empresa"
                            @input="${this.updateYearsInCompany}"
                            .value="${this.person.yearsInCompany}" />
                    </div>
                    <button @click="${this.goBack}" class="btn btn-primary"><strong>Atrás</strong></button>
                    <button @click="${this.storePerson}" class="btn btn-success"><strong>Guardar</strong></button>
                </form>
            </div>            
        `;
    }

    updated(changedProperties) {
        console.log('person-form - updated');
        if(changedProperties.has('person')) {
            console.log(`person-form - updated - Propiedad person ha cambiado de valor`);
        }
    }

    updateName(e) {
        console.log('person-form - updateName');
        console.log('person-form - updateName - Actualizando la propiedad name con el valor ' + e.target.value);
        this.person.name = e.target.value;
    }

    updateProfile(e) {
        console.log('person-form - updateProfile');
        console.log('person-form - updateProfile - Actualizando la propiedad profile con el valor ' + e.target.value);
        this.person.profile = e.target.value;
    }

    updateYearsInCompany(e) {
        console.log('person-form - updateYearsInCompany');
        console.log('person-form - updateYearsInCompany - Actualizando la propiedad yearsInCompany con el valor ' + e.target.value);
        this.person.yearsInCompany = e.target.value;
    }

    goBack(e) {
        console.log('person-form - goBack');
        e.preventDefault();

        this.dispatchEvent(new CustomEvent('persona-form-close', {}));

        this.resetFormData();
    }

    resetFormData() {
        console.log('person-form - resetFormData');

        this.person = {};
        this.person.name = '';
        this.person.profile = '';
        this.person.yearsInCompany = '';

        this.editingPerson = false;
    }

    storePerson(e) {
        console.log('person-form - storePerson');
        e.preventDefault();

        let photo = {
            src: './img/persona.png',
            alt: 'Persona'
        }

        this.dispatchEvent(new CustomEvent('persona-form-store', {
            detail: {
                person: {
                    name: this.person.name,
                    profile: this.person.profile,
                    yearsInCompany: this.person.yearsInCompany,
                    photo: this.person.photo ? this.person.photo : photo
                },
                editingPerson: this.editingPerson
            }
        }));
    }
}

customElements.define('persona-form', PersonaForm);
