import { LitElement, html, css } from 'lit-element';
import '../persona-ficha-listado/persona-ficha-listado.js';
import '../persona-form/persona-form.js';
import '../persona-main-dm/persona-main-dm.js'

class PersonaMain extends LitElement {

    static get styles() {
        return css `
            :host {
                all: initial;
            }
        `;
    }

    static get properties() {
        return {
            people: { type: Array },
            showPersonForm: { type: Boolean },
            range: { type: Number }
        };
    }

    constructor() {
        super();

        this.people = [];

        this.showPersonForm = false;
    }

    render() {
        return html `
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
            <h2 class="text-center">Personas</h2>
            <div class="row" id="peopleList">
                <div class="row row-cols-1 row-cols-sm-4">
                    ${this.people.filter(
                        person => person.yearsInCompany >= this.range
                    ).map(
                        person => html `
                            <persona-ficha-listado
                                fname="${person.name}"
                                yearsInCompany="${person.yearsInCompany}"
                                profile="${person.profile}"
                                .photo="${person.photo}"
                                @delete-person="${this.deletePerson}"
                                @info-person="${this.infoPerson}">
                            </persona-ficha-listado>`
                    )}
                </div>
            </div>
            <div class="row">
                <persona-form id="personForm" class="border rounded border-primary"
                    @persona-form-close="${this.personFormClose}"
                    @persona-form-store="${this.personFormStore}"></persona-form>
            </div>
            <persona-main-dm @updated-people="${this.updatedPeople}"></persona-main-dm>
        `;
    }

    updated(changedProperties) {
        console.log('persona-main - updated');
        if(changedProperties.has('showPersonForm')) {
            console.log(`persona-main - updated - Propiedad showPersonForm ha cambiado de valor, el anterior era ${changedProperties.get('showPersonForm')} y el nuevo es ${this.showPersonForm}`);
            if(this.showPersonForm === true) {
                this.showPersonFormData();
            } else {
                this.showPersonList();
            }
        }
        if(changedProperties.has('people')) {
            console.log('persona-main - updated - Ha cambiado la propiedad people');

            this.dispatchEvent(new CustomEvent('updated-people', {
                detail: {
                    people: this.people
                }
            }));
        }
        if(changedProperties.has('range')) {
            console.log('persona-main - updated - Ha cambiado la propiedad range - ' + this.range);
        }
    }

    personFormClose(e) {
        console.log('persona-main - personFormClose');
        this.showPersonForm = false;
    }

    personFormStore(e) {
        console.log('persona-main - personFormStore');
        console.log(e);
        console.log(e.detail.person);
        console.log('persona-main - personFormStore - editingPerson - ' + e.detail.editingPerson);

        if (e.detail.editingPerson === true) {
            console.log('persona-main - personFormStore - Se va a actualizar la persona de nombre ' + e.detail.person.name);

            this.people = this.people.map(
                person => person.name === e.detail.person.name ? person = e.detail.person : person
            );
        } else {
            console.log('persona-main - personFormStore - Se va a guardar una persona nueva');
            this.people = [...this.people, e.detail.person];
        }

        this.showPersonForm = false;
    }

    showPersonList() {
        console.log('persona-main - showPersonList');
        console.log('persona-main - showPersonList - Mostrando el listado de personas');
        this.shadowRoot.getElementById('peopleList').classList.remove('d-none');
        this.shadowRoot.getElementById('personForm').classList.add('d-none');
    }

    showPersonFormData() {
        console.log('persona-main - showPersonFormData');
        console.log('persona-main - showPersonFormData - Mostrando el formulario de persona');
        this.shadowRoot.getElementById('peopleList').classList.add('d-none');
        this.shadowRoot.getElementById('personForm').classList.remove('d-none');
    }

    deletePerson(e) {
        console.log('persona-main - deletePerson');
        console.log('persona-main - deletePerson - Se va a borrar la persona de nombre ' + e.detail.name);

        this.people = this.people.filter(
            person => person.name != e.detail.name
        );
    }

    infoPerson(e) {
        console.log('persona-main - infoPerson');
        console.log('persona-main - infoPerson - Se va a consultar la persona de nombre ' + e.detail.name);

        let chosenPerson = this.people.filter(
            person => person.name === e.detail.name
        );
        let person = {};
        person.name = chosenPerson[0].name;
        person.profile = chosenPerson[0].profile;
        person.yearsInCompany = chosenPerson[0].yearsInCompany;
        person.photo = chosenPerson[0].photo;
        console.log(person);

        this.shadowRoot.getElementById('personForm').person = person;
        this.shadowRoot.getElementById('personForm').editingPerson = true;

        this.showPersonForm = true;
    }

    updatedPeople(e) {
        console.log('persona-main - updatedPeople');

        this.people = e.detail.people;
    }
}

customElements.define('persona-main', PersonaMain);
