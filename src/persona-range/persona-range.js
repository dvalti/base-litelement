import { LitElement, html } from 'lit-element';

class PersonaRange extends LitElement {

    static get properties() {
        return {
            range: { value: Number },
            maxYearsInCompany: { value: Number }
        };
    }

    constructor() {
        super();

        this.range = 0;
    }

    render() {
        return html `
            <input type="range" min="0" .max="${this.maxYearsInCompany}" step="1"
                .value="${this.range}"
                @change="${this.rangeChange}">
        `;
    }

    updated(changedProperties) {
        console.log('persona-range - updated');

        if (changedProperties.has('range')) {
            console.log('persona-range - Ha cambiado la propiedad range - ' + this.range);

            this.dispatchEvent(new CustomEvent('updated-people-range', {
                detail: {
                    range: this.range
                }
            }));
        }
    }

    rangeChange(e) {
        console.log('persona-range - rangeChange');
        console.log(e.target.value);

        this.range = e.target.value;
    }
}

customElements.define('persona-range', PersonaRange);
