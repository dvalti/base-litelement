import { LitElement, html } from 'lit-element';
import '../persona-range/persona-range.js';

class PersonaSidebar extends LitElement {

    static get properties() {
        return {
            peopleStats: { type: Object }
        };
    }

    constructor() {
        super();

        this.peopleStats = {};
    }

    render() {
        return html `
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
            <aside>
                <section>
                    <div>
                        Hay <span class="badge bg-pill bg-primary">${this.peopleStats.numberOfPeople}</span> personas
                    </div>
                    <div>
                        Filtro por años en la empresa
                        <persona-range @updated-people-range="${this.updatedPeopleRange}" .maxYearsInCompany="${this.peopleStats.maxYearsInCompany}"></persona-range>
                    </div>
                    <div class="mt-5">
                        <button class="w-100 btn bg-success" style="font-size: 50px"
                            @click="${this.newPerson}">
                            <strong>+</strong>
                        </button>
                    </div>
                </section>
            </aside>
        `;
    }

    updated(changedProperties) {
        console.log('persona-sidebar - updated');

        if (changedProperties.has('peopleStats')) {
            console.log('persona-sidebar - Ha cambiado la propiedad peopleStats');

            console.log(this.peopleStats);
        }
    }

    newPerson(e) {
        console.log('persona-sidebar - newPerson');
        console.log('persona-sidebar - newPerson - Se va a crear una persona');

        this.dispatchEvent(new CustomEvent('new-person', {}));
    }

    updatedPeopleRange(e) {
        console.log('persona-sidebar - updatedPeopleRange');
        console.log('persona-sidebar - updatedPeopleRange - ' + e.detail.range);

        this.dispatchEvent(new CustomEvent('updated-people-range', e));
    }
}

customElements.define('persona-sidebar', PersonaSidebar);
