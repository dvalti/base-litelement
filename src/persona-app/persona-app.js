import { LitElement, html } from 'lit-element';
import '../persona-header/persona-header.js';
import '../persona-main/persona-main.js';
import '../persona-footer/persona-footer.js';
import '../persona-sidebar/persona-sidebar.js';
import '../persona-stats/persona-stats.js';

class PersonaApp extends LitElement {

    static get properties() {
        return {
            people: { type: Array },
            range: { type: Number }
        };
    }

    constructor() {
        super();
    }

    render() {
        return html `
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
            <persona-header></persona-header>
            <div class="row">
                <persona-sidebar @new-person="${this.newPerson}" @updated-people-range="${this.updatedPeopleRange}" class="col-2"></persona-sidebar>
                <persona-main @updated-people="${this.updatedPeople}" class="col-10"></persona-main>
            </div>
            <persona-footer></persona-footer>
            <persona-stats @updated-people-stats="${this.updatedPeopleStats}"></persona-stats>
        `;
    }

    updated(changedProperties) {
        console.log('persona-app - updated');

        if (changedProperties.has('people')) {
            console.log('persona-app - updated - Ha cambiado la propiedad people');

            this.shadowRoot.querySelector('persona-stats').people = this.people;
        }
        if (changedProperties.has('range')) {
            console.log('persona-app - updated - Ha cambiado la propiedad range - ' + this.range);

            this.shadowRoot.querySelector('persona-main').range = this.range;
        }
    }

    newPerson(e) {
        console.log('persona-app - newPerson');

        this.shadowRoot.querySelector('persona-main').showPersonForm = true;
    }

    updatedPeople(e) {
        console.log('persona-app - updatedPeople en persona-app');

        this.people = e.detail.people;
    }

    updatedPeopleStats(e) {
        console.log('persona-app - updatedPeopleStats');
        console.log(e.detail.peopleStats.numberOfPeople);
        console.log(e.detail.peopleStats.maxYearsInCompany);

        this.shadowRoot.querySelector('persona-sidebar').peopleStats = e.detail.peopleStats;
    }

    updatedPeopleRange(e) {
        console.log('persona-app - updatedPeopleRange');
        console.log('persona-app - updatedPeopleRange - ' + e.detail.range);

        this.range = e.detail.range;
    }
}

customElements.define('persona-app', PersonaApp);
