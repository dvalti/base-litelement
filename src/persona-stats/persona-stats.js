import { LitElement, html } from 'lit-element';

class PersonaStats extends LitElement {

    static get properties() {
        return {
            people: { type: Array }
        };
    }

    constructor() {
        super();

        this.people = [];
    }

    render() {
        return html `
            <h1>PersonaStats</h1>
        `;
    }

    updated(changedProperties) {
        console.log('persona-stats - updated');

        if (changedProperties.has('people')) {
            console.log('persona-stats - updated - Ha cambiado la propiedad people en persona-stats');

            let peopleStats = this.gatherPeopleArrayInfo(this.people);
            this.dispatchEvent(new CustomEvent('updated-people-stats', {
                detail: {
                    peopleStats: peopleStats
                }
            }));
        }
    }

    gatherPeopleArrayInfo(people) {
        console.log('persona-stats - gatherPeopleArrayInfo');

        let peopleStats = {};
        peopleStats.numberOfPeople = people.length;

        let maxYearsInCompany = this.people.reduce((max, people) => people.yearsInCompany > max ? people.yearsInCompany : max, 0);
        console.log('persona-stats - gatherPeopleArrayInfo - maxYearsInCompany - ' + maxYearsInCompany);

        peopleStats.maxYearsInCompany = maxYearsInCompany;

        return peopleStats;
    }
}

customElements.define('persona-stats', PersonaStats);
