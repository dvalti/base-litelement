import { LitElement, html } from 'lit-element';

class FichaPersona extends LitElement {

    static get properties() {
        return {
            name: { type: String },
            yearsInCompany: { type: Number },
            personInfo: { type: String }
        };
    }

    constructor() {
        super();
        this.name = 'Prueba nombre';
        this.yearsInCompany = 12;
    }

    updated(changedProperties) {
        console.log('updated');

        changedProperties.forEach((oldValue, propName) => {
            console.log(`Propiedad ${propName} cambia valor, anterior era ${oldValue}`);
        });

        if(changedProperties.has('name')) {
            console.log(`Propiedad name ha cambiado de valor, el anterior era ${changedProperties.get('name')} y el nuevo es ${this.name}`);
        }

        if(changedProperties.has('yearsInCompany')) {
            console.log(`Propiedad yearsInCompany ha cambiado de valor, el anterior era ${changedProperties.get('yearsInCompany')} y el nuevo es ${this.yearsInCompany}`);
            this.updatePersonInfo();
        }
    }

    render() {
        return html `
            <div>
                <label>Nombre completo</label>
                <input type="text" id="fname" name="fname" value="${this.name}" @input="${this.updateName}" />
                <br />
                <label>Años en la empresa</label>
                <input type="text" id="yearsInCompany" name="yearsInCompany" value="${this.yearsInCompany}" @input="${this.updateYearsInCompany}" />
                <br />
                <input type="text" value="${this.personInfo}" disabled />
                <br />
            </div>
        `;
    }

    updateName(e) {
        console.log('updateName');
        this.name = e.target.value;
    }

    updateYearsInCompany(e) {
        console.log('updateName');
        this.yearsInCompany = e.target.value;
    }

    updatePersonInfo() {
        if(this.yearsInCompany >= 7) {
            this.personInfo = 'lead';
        } else if(this.yearsInCompany >= 5) {
            this.personInfo = 'senior';
        } else if(this.yearsInCompany >= 3) {
            this.personInfo = 'team';
        } else {
            this.personInfo = 'junior';
        }
    }
}

customElements.define('ficha-persona', FichaPersona);
